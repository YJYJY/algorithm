/************************************************************************/
/*        
*		     char * strstr ( const char *, const char * );
*	     a字符串里 查看是否有b字符串，有则 从首次发现b字符串处 返回 a字符串。没有则输出 null
*/
/************************************************************************/

#include <stdio.h>
#include <string.h>
#include <assert.h>


//用于查找 str2 在 str1 中第一次出现的位置，找到则返回，否则返回 NULL,也是逐个比较
char* strstr_my(char* str1,char* str2)
{
	assert(str1 != NULL && str2 != NULL);

	for (int i = 0; str1[i] != '\0'; i++)
	{
		int temp = i;
		int j = 0;
		while (str1[i++] == str2[j++])
		{
			if (str2[j] == '\0')
			{
				return &str1[temp];
			}
			else if (str1[i] == '\0')
			{
				return NULL;
			}
		}
		i = temp;
	}

	return NULL;
}



int main()
{
	char* strSrc = "dsalkdjlwqyinjsad245";
	char* strSub = "yinj";

	printf("the sub:%s \n", strstr_my(strSrc, strSub));

	int a = 6;
	printf("sizeof(a++) = %d %d\n", sizeof(a++), a);
	printf("sizeof(a+1.0) = %d \n", sizeof(a + 1.0));

	getchar();

	return 0;
}