/************************************************************************/
/*     一、strcmp 函数介绍
	 extern int strcmp(const char *s1,const char * s2);
	 比较两个字符串。设这两个字符串为str1，str2，若str1=str2，则返回零；若str1>str2，则返回正数；若str1<str2，则返回负数。

就是对两个字符串从左到右逐个字符相比，按ASCII值大小比较，如果出现不同字符或遇到'\0'为止，如果全部字符相等；则认为相等，如果不相等的，按第一个不相等的字符的比较结果为准，
如strmp（str，str），则函数值为0，如果strmp（stra，strb）因为a的askii小于b的，所以函数值为一个负数。
                                                                 */
/************************************************************************/

#include <stdio.h>
#include <string.h>
#include <assert.h>


int strcmp_my(const char *str1, const char *str2)
{
	/*不可用while(*str1++==*str2++)来比较，当不相等时仍会执行一次++，
	return返回的比较值实际上是下一个字符。应将++放到循环体中进行。*/
	assert(str1 != NULL && str2 != NULL);

	while (*str1 == *str2)
	{
		if (*str1 == '\0')
			return 0;

		str1++;
		str2++;
	}
	return *str1 - *str2;
}


int main()
{
	printf("test 1 :%d \n", strcmp_my("123","123"));
	printf("test 2 :%d \n", strcmp_my("asd", "bd"));
	printf("test 3 :%d \n", strcmp_my("sd", "bd"));

	getchar();

	return 0;
}