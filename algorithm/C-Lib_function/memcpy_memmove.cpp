/************************************************************************/
/* memcpy 和 memmove 的实现                     
* void *memcpy(void *dst, const void *src, size_t count);
*void *memmove(void *dst, const void *src, size_t count);
*他 们的作用是一样的，唯一的区别是，当内存发生局部重叠的时候，memmove保证拷贝的结果是正确的，memcpy不保证拷贝的结果的正确
*/
/************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include<assert.h>


char* strcpy_my(const char* src, char* des)
{
	assert((src != NULL) && (des != NULL));
	char* temp = des;
	while ((*des++ = *src++) != '\0')
		NULL;
	return temp;
}


void *memcpy_my(void *dest, const void *source, size_t count)
{
	assert((NULL != dest) && (NULL != source));
	char *tmp_dest = (char *)dest;												// 注意此处转换为 char 类型
	char *tmp_source = (char *)source;
	while (count--)																		//不对是否存在重叠区域进行判断
		*tmp_dest++ = *tmp_source++;
	return dest;
}


void *memmove_my(void *dest, const void *source, size_t count)
{
	assert((NULL != dest) && (NULL != source));
	char *tmp_dest = (char *)dest;             // 注意此处转换为 char 类型
	char *tmp_source = (char *)source;

	if ((dest <= source) || ((tmp_source + count) < dest))
	{// 如果没有重叠区域
		while (count--)
			*tmp_dest++ = *tmp_source++;
	}
	else
	{ //如果有重叠，保证在源串src被覆盖之前将重叠区域的字节拷贝到目标区域中
	  // 因此考虑采用先拷贝最后一个字节，依次回退；
		tmp_source += count - 1;
		tmp_dest += count - 1;
		while (count--)
			*--tmp_dest = *--tmp_source;
	}
	return dest;
}


int main()
{
	char strTestDate[32] = { 0 };
	memcpy_my(strTestDate, "234234", 6);
	printf("%s \n", strTestDate);

	return 0;
}