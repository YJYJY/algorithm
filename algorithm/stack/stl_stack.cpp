/************************************************************************/
/* stack（栈）  和 queue （队列是一个类型的，先进先出）
*配接器
*特点：				先进后出
*实现：				通过序列容器vector、deque或者list对象来实现
*/
/************************************************************************/

#include <stack>

using namespace std;


int main()
{
	stack<int>  sInt;
	sInt.push(10);							// 根据stack 的特点，所有只有 push 和 pop 两种操作
	sInt.pop();

	sInt.size();									// 当前堆栈的大小

	int mytop = sInt.top();				// 栈顶元素

	sInt.empty();								// 判断堆栈是否为空
	return 0;
}