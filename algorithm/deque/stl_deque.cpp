/************************************************************************/
/*  deque（double-ended queue 双向队列）
*	序列容器
*  特点：							①一种随机访问的数据类型，动态数组	
*										②无论是在头还是尾，删除和插入都很快
*  实现方式：					双向开口的连续性存储空间，deque要慎用
*  使用场景：
* 与其他容器比较:		    ① 和 vector， vector 在插入序列时，尾端操作是高效的； deque 是两端；
*/
/************************************************************************/

#include<deque>
#include <iostream>

using namespace std;

int main()
{
	deque<int> dInt;
	dInt.push_back(1);						// 序列尾端插入元素
	dInt.pop_back();							// 尾端删除元素
	dInt.push_front(2);						//  前端 插入元素
	dInt.pop_front();							//  前端删除元素 ， 这四种操作都很快的

	dInt.push_front(1);
	dInt.push_front(2);
	dInt.push_front(3);

	deque<int>::iterator dIter = dInt.begin();
	dIter++;																		// 从中间某个位置插入元素，需要进行移动操作，慢
	dInt.insert(dIter, 2,15);													// 插入 两个 15 
	for (dIter = dInt.begin(); dIter != dInt.end(); dIter++)
	{
		cout << *dIter << endl;
	}


	dInt.erase(dInt.begin());         // 删除特定元素
	dInt.clear();							 // 删除所有元素

	getchar();

	return 0;
}