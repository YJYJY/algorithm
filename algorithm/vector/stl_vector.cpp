/************************************************************************/
/* 
* 序列性容器 （容器中的元素是 可序的（元素集合呈线性关系排列），但并未排序，其实就是无序的）
* vector 动态数组， 相比于数组的优势在不必在一开始就分别 一个很大的内存块
* 特点：大小可变
*			①对数组元素的快速、随机访问
*	        ② 在 尾部 的 快速、随机 插入和删除
*			③ 元素占用 连续空间
*
*存储结构：顺序，连续
*/
/************************************************************************/

#include <stdio.h>
#include<vector>
#include <iostream>
#include <windows.h>


using namespace std;

/************************************************************************/
/* 测试vector 的基本操作                                                                     */
/************************************************************************/
void Test_base()
{
	vector<int> vInt;
	vInt.push_back(1);                           //  在尾部插入和删除元素，很快
	vInt.pop_back();

	vInt.push_back(1);
	vInt.push_back(2);
	vInt.push_back(3);
	vInt.push_back(4);

	// 没有 直接从头插入的操作， push_front 和 pop_front , 只能使用insert  需要进行元素移动，比较慢

	//insert
	vector<int>::iterator inIter = vInt.begin();
	vInt.insert(inIter, 1, 10);											// 从头开始插入,需要进行移动元素

	for (inIter = vInt.begin(); inIter != vInt.end(); inIter++)
	{
		cout << *inIter << endl;
	}

	//删除元素
	inIter = vInt.begin();
	vInt.erase(inIter);							// 删除特定元素
	for (inIter = vInt.begin(); inIter != vInt.end(); inIter++)
	{
		cout << *inIter << endl;
	}

	vInt.clear();							// 删除所有元素
}

/************************************************************************/
/*  静态数组 定义 数组失败,
*①使用 vector 来替换，初始化成功   int temp = 10;  vector<char> mytest(temp);
*②使用 堆上进行分配，记得释放	  int temp = 10;  char* mytest = new char[temp];   delete[] mytest;
*/
/************************************************************************/
void Test_Struct()
{
	typedef struct
	{
		DWORD                           nNum;                               // 设备数量
		char									 test[16];							// 数组测试
		void*								 pInfor;                             // 具体设备信息
	}DEVICE_INFORMATION;

	DEVICE_INFORMATION devInfo;
	memset(&devInfo, 0, sizeof(DEVICE_INFORMATION));
	devInfo.nNum = 10;
	const int temp = devInfo.nNum;

	//	char rere[temp];															 // 初始化失败，但是在Linux下的 gcc 编译器却又成功

	vector<char> rere(devInfo.nNum);														// 10 zero-initialized ints,初始化 大小
//	rere.reserve(devInfo.nNum);											//指定vector的大小
//	memset(&rere,0,sizeof(char)*devInfo.nNum);

	cout << rere.size() << endl;							// 返回当前元素的数量
	cout<< rere.max_size()<< endl;						// 可容纳的元素最大数量
	cout << rere.capacity() << endl;					// 返回重新分配前所能容纳的元素最大数目


	rere[0] = 12;
	rere[1] = 32;
	rere[3] = 42;
	rere[4] = 42;
	rere[5] = 42;
	rere[6] = 42;
	rere[7] = 42;
	rere[8] = 42;

	for (int index =0; index<temp;index++)
	{
		cout <<index <<"---" << rere[index] << endl;
	}


	int dsa = rere.at(1);                                                  //必须先初始化 vector的大小，size有初始值才可以使用 at

//	rere.at(0) = 7;
	rere.push_back(43);										// push_back 才会改变，vector 的大小，直接通过数组形式复制无法改变 vector大小
	cout << rere.size() << endl;

	int i = 0;
	char* array = &rere[i];

	cout << *array << endl;


	vector<DEVICE_INFORMATION> vSt(10);
	cout << vSt.size() << endl;
	cout << vSt.max_size() << endl;
	cout << vSt.capacity() << endl;

	vSt[0].nNum = 432;
	memcpy(vSt[0].test,"yijn",4);
	vSt[1].nNum = 2;
	vSt[2].nNum = 54;

	DEVICE_INFORMATION* dada = &vSt[0];
	
	cout << dada->nNum << "---------------" << dada->test << endl;

}




int main()
{
	
	Test_Struct();
	getchar();
	return 0;
}