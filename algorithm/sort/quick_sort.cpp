/************************************************************************/
/* 快速排序法, 感觉就是折半排序
*该方法的基本思想是：
1．先从数列中取出一个数作为基准数。
2．分区过程，将比这个数大的数全放到它的右边，小于或等于它的数全放到它的左边。
3．再对左右区间重复第二步，直到各区间只有一个数。

更细一步的伪代码：
1．i =L; j = R; 将基准数挖出形成第一个坑a[i]。
2．j--由后向前找比它小的数，找到后挖出此数填前一个坑a[i]中。
3．i++由前向后找比它大的数，找到后也挖出此数填到前一个坑a[j]中。
4．再重复执行2，3二步，直到i==j，将基准数填入a[i]中。

参考资料：
http://blog.csdn.net/morewindows/article/details/6684558
*/
/************************************************************************/

#include<stdio.h>

void PrintfArray(int s[], int nSize)
{
	for (int i = 0; i < nSize; i++)
	{
		printf("s[%d]:%d \n", i, s[i]);
	}
}

int AdjustArray(int s[], int nleft, int nright) //返回调整后基准数的位置  
{
	int i = nleft, j = nright;
	int x = s[nleft];							//s[l]即s[i]就是第一个坑  
	while (i < j)
	{
		// 从右向左找小于x的数来填s[i]  
		while (i < j && s[j] >= x)
			j--;

		if (i < j)
		{
			s[i] = s[j];							//将s[j]填到s[i]中，s[j]就形成了一个新的坑  
			i++;
		}

		// 从左向右找大于或等于x的数来填s[j]  
		while (i < j && s[i] < x)
			i++;
		if (i < j)
		{
			s[j] = s[i];						  //将s[i]填到s[j]中，s[i]就形成了一个新的坑  
			j--;
		}
	}
	//退出时，i等于j。将x填到这个坑中。  
	s[i] = x;

	return i;
}

void quick_sort1(int s[], int nleft, int nright)
{
	if (nleft < nright)
	{
		int i = AdjustArray(s, nleft, nright);		//先成挖坑填数法调整s[] ,返回的i 就是第一个位置的值 
		PrintfArray(s, nright - nleft + 1);
		quick_sort1(s, nleft, i - 1);				// 递归调用   
		quick_sort1(s, i + 1, nright);
	}
}

int main()
{
	int nTest[10] = { 231,34,623,4,75,8,976,24,7657,10 };
	quick_sort1(nTest, 0, 9);												//从 数组下标开始 

	PrintfArray(nTest,10);

	getchar();
	return 0;
}
