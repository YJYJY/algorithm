/************************************************************************/
/*   insert_sort 插入排序法                        
*	分为 线性插入排序 和 折半插入排序法
基本思想：
	反复地插入新元素到已经排好序的列表中，且令插入后的列表也是有序的；

*/
/************************************************************************/


#include<stdio.h>

void PrintfArray(int s[], int nSize)
{
	for (int i = 0; i < nSize; i++)
	{
		printf("s[%d]:%d \n", i, s[i]);
	}
}


static void insertion_sort(int unsorted[],int length)
{
	for (int i = 1; i < length; i++)															// 从第一个开始
	{
		if (unsorted[i - 1] > unsorted[i])
		{
			int temp = unsorted[i];
			int j = i;
			while (j > 0 && unsorted[j - 1] > temp)
			{
				unsorted[j] = unsorted[j - 1];
				j--;
			}
			unsorted[j] = temp;
		}
	}
}


int main()
{
	int nTest[10] = { 231,34,623,4,75,8,976,24,7657,10 };
//	insert_sort(nTest, 10, true);				// 升序
	insertion_sort(nTest,10);
	PrintfArray(nTest, 10);
//	bubble_sort(nTest, 10, false);			// 降序
//	PrintfArray(nTest, 10);
	getchar();

	return 0;
}