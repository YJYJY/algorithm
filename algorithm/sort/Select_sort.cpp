/************************************************************************/
/* select_sort 选择排序法     
基本思想：
	升序排列：假设把第一个元素设置为最小，然后从后面的元素中找到最小的元素。也是进行两次循环。
	注意与 交互排序法（冒泡法）的区别

*/
/************************************************************************/

#include<stdio.h>

void PrintfArray(int s[], int nSize)
{
	for (int i = 0; i < nSize; i++)
	{
		printf("s[%d]:%d \n", i, s[i]);
	}
}

void Select_sort(int data[], int nlen,bool bdire)
{
	int i, j, k, temp;
	for (i = 0; i < nlen; i++)						//取一个元素记为最小的，与第一个元素交换位置，从而有序
	{
		k = i;
		for (j = i + 1; j < nlen; j++)				//从剩下的无序区继续取元素，与第一个位置的元素比较
		{
			bool bcom = bdire ? (data[j] < data[k]) : (data[j] > data[k]);
			if (bcom)
				k = j;
		}
			
		if (k != i)									//如果第一个位置不是最小的，则交换这个位置
		{
			temp = data[i];
			data[i] = data[k];
			data[k] = temp;
		}
	}
}


int main()
{
	int nTest[10] = { 231,34,623,4,75,8,976,24,7657,10 };
	Select_sort(nTest, 10,true);				// 升序
	PrintfArray(nTest, 10);
	Select_sort(nTest, 10, false);			// 降序
	PrintfArray(nTest, 10);
	getchar();

	return 0;
}