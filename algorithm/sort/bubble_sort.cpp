/************************************************************************/
/* bubble_sort 冒泡排序法，最简单的一种排序        
基本思想：
	①冒泡排序就是把小的元素往前调或者把大的元素往后调。
	②比较是相邻的两个元素比较，交换也发生在这两个元素之间。
	③相同元素的前后顺序不会发生变化，冒泡法是一种稳定的排序算法。

*/
/************************************************************************/

#include<stdio.h>

void PrintfArray(int s[], int nSize)
{
	for (int i = 0; i < nSize; i++)
	{
		printf("s[%d]:%d \n", i, s[i]);
	}
}

void bubble_sort(int array[], int length,bool bdir)      
{
	int i, j, temp;
	for (i = 0; i < length - 1; i++)					  // 每轮过后，最后一个元素都是最大或者最小的
		for (j = 0; j < length - i - 1; j++)           // 注意此处的长度会减少    
		{
			bool bcom = bdir ? (array[j] > array[j + 1]) : (array[j] < array[j + 1]);
			if (bcom)				
			{
				temp = array[j];
				array[j] = array[j + 1];
				array[j + 1] = temp;
			}
		}
}

int main()
{
	int nTest[10] = { 231,34,623,4,75,8,976,24,7657,10 };
	bubble_sort(nTest, 10,true);				// 升序
	PrintfArray(nTest,10);
	bubble_sort(nTest, 10, false);			// 降序
	PrintfArray(nTest, 10);
	getchar();

	return 0;
}