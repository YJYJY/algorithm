/************************************************************************/
/* http://blog.csdn.net/fansongy/article/details/6798278
http://blog.csdn.net/sjf0115/article/details/8645991
http://blog.csdn.net/csdn0123/article/details/7424054
*binary tree(二叉树)
*		定义：			任何节点 最多只允许两个子节点
*      性质：			①非空二叉树的第n层上至多有2^(n-1)个元素。
*							②深度为h的二叉树至多有2^h-1个结点。
*		存储结构： 顺序存储，将数据结构存在一块固定的数组中；
*							优点：遍历速度快； 缺点：占用存储空间大
*							链式存储：
*
*
*	满二叉树
*		定义：			一颗 每个节点上都有左右子树的 二叉树；
*		性质：			由二叉树的性质可以推导出
*							①满二叉树的第n层上一定有个 2^(n-1)个元素；
*							②深度为h 的满二叉树一定有 2^h-1个结点；
*		存储结构：	因为所有的位置，所有采用的是顺序存储结构，不采用链式
*
*	完全二叉树
*		定义：			n层完全二叉树是将n层满二叉树最后一层从后向前依次去处少于2^n个元素； 
*      性质：			和满二叉树的差别就是 最后一层的元素
*/
/************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stack>
#include <queue>
#include <iostream>

using namespace std;

/*******顺序存储***********/
#define LENGTH 100
typedef char datatype;
typedef struct node {
	datatype data;
	int lchild, rchild;
	int parent;
}Node;

Node tree[LENGTH];
int length;
int root;


/**********链式存储*****************/
//二叉树结点
typedef struct BiTNode {
	//数据
	char data;
	//左右孩子指针
	struct BiTNode *lchild, *rchild;
}BiTNode, *BiTree;


//按先序序列创建二叉树
int CreateBiTree(BiTree &T)
{
	char data;
	//按先序次序输入二叉树中结点的值（一个字符），‘#’表示空树
	scanf("%c", &data);
	if (data == '#') {
		T = NULL;
	}
	else {
		T = (BiTree)malloc(sizeof(BiTNode));
		//生成根结点
		T->data = data;
		//构造左子树
		CreateBiTree(T->lchild);
		//构造右子树
		CreateBiTree(T->rchild);
	}
	return 0;
}

/************************************************************************/
/*  递归算法，遍历                                                                     */
/************************************************************************/
//输出
void Visit(BiTree T)
{
	if (T->data != '#') {
		printf("%c ", T->data);
	}
}

//先序遍历， 根左右
void PreOrder(BiTree T) 
{
	if (T != NULL) {
		//访问根节点
		Visit(T);
		//访问左子结点
		PreOrder(T->lchild);
		//访问右子结点
		PreOrder(T->rchild);
	}
}

//中序遍历  左根右
void InOrder(BiTree T) 
{
	if (T != NULL) {
		//访问左子结点
		InOrder(T->lchild);
		//访问根节点
		Visit(T);
		//访问右子结点
		InOrder(T->rchild);
	}
}

//后序遍历  左右根
void PostOrder(BiTree T) {
	if (T != NULL) {
		//访问左子结点
		PostOrder(T->lchild);
		//访问右子结点
		PostOrder(T->rchild);
		//访问根节点
		Visit(T);
	}
}


/* 先序遍历(非递归)
思路：访问T->data后，将T入栈，遍历左子树；遍历完左子树返回时，栈顶元素应为T，出栈，再先序遍历T的右子树。
*/
void PreOrder2(BiTree T) {
	stack<BiTree> stack;
	//p是遍历指针  
	BiTree p = T;
	//栈不空或者p不空时循环  
	while (p || !stack.empty()) {
		if (p != NULL) {
			//存入栈中  
			stack.push(p);
			//访问根节点  
			printf("%c ", p->data);
			//遍历左子树  
			p = p->lchild;
		}
		else {
			//退栈  
			p = stack.top();
			stack.pop();
			//访问右子树  
			p = p->rchild;
		}
	}//while  
}
/* 中序遍历(非递归)
思路：T是要遍历树的根指针，中序遍历要求在遍历完左子树后，访问根，再遍历右子树。
先将T入栈，遍历左子树；遍历完左子树返回时，栈顶元素应为T，出栈，访问T->data，再中序遍历T的右子树。
*/
void InOrder2(BiTree T) {
	stack<BiTree> stack;
	//p是遍历指针  
	BiTree p = T;
	//栈不空或者p不空时循环  
	while (p || !stack.empty()) {
		if (p != NULL) {
			//存入栈中  
			stack.push(p);
			//遍历左子树  
			p = p->lchild;
		}
		else {
			//退栈，访问根节点  
			p = stack.top();
			printf("%c ", p->data);
			stack.pop();
			//访问右子树  
			p = p->rchild;
		}
	}//while  
}

//后序遍历(非递归)  
typedef struct BiTNodePost {
	BiTree biTree;
	char tag;
}BiTNodePost, *BiTreePost;

void PostOrder2(BiTree T) {
	stack<BiTreePost> stack;
	//p是遍历指针  
	BiTree p = T;
	BiTreePost BT;
	//栈不空或者p不空时循环  
	while (p != NULL || !stack.empty()) {
		//遍历左子树  
		while (p != NULL) {
			BT = (BiTreePost)malloc(sizeof(BiTNodePost));
			BT->biTree = p;
			//访问过左子树  
			BT->tag = 'L';
			stack.push(BT);
			p = p->lchild;
		}
		//左右子树访问完毕访问根节点  
		while (!stack.empty() && (stack.top())->tag == 'R') {
			BT = stack.top();
			//退栈  
			stack.pop();
			printf("%c ", BT->biTree->data);
		}
		//遍历右子树  
		if (!stack.empty()) {
			BT = stack.top();
			//访问过右子树  
			BT->tag = 'R';
			p = BT->biTree;
			p = p->rchild;
		}
	}//while  
}
//层次遍历  
void LevelOrder(BiTree T) {
	BiTree p = T;
	//队列  
	queue<BiTree> queue;
	//根节点入队  
	queue.push(p);
	//队列不空循环  
	while (!queue.empty()) {
		//对头元素出队  
		p = queue.front();
		//访问p指向的结点  
		printf("%c ", p->data);
		//退出队列  
		queue.pop();
		//左子树不空，将左子树入队  
		if (p->lchild != NULL) {
			queue.push(p->lchild);
		}
		//右子树不空，将右子树入队  
		if (p->rchild != NULL) {
			queue.push(p->rchild);
		}
	}
}


int main()
{
	BiTree T;
	CreateBiTree(T);
	printf("先序遍历：\n");
	PreOrder(T);
	printf("\n");
	printf("先序遍历(非递归)：\n");
	PreOrder2(T);
	printf("\n");
	printf("中序遍历：\n");
	InOrder(T);
	printf("\n");
	printf("中序遍历(非递归)：\n");
	InOrder2(T);
	printf("\n");
	printf("后序遍历：\n");
	PostOrder(T);
	printf("\n");
	printf("后序遍历(非递归)：\n");
	PostOrder2(T);
	printf("\n");
	printf("层次遍历：\n");
	LevelOrder(T);
	printf("\n");
	getchar();
	return 0;
}

