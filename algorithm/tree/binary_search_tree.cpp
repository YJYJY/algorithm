﻿/************************************************************************/
/*   binary tree(二叉树)
*		定义：任何节点 最多只允许两个子节点
*
*http://blog.csdn.net/whucyl/article/details/17100225
*	 binary search tree (二叉查找树、二叉搜索树) 
*		定义:		任意节点的键值一定大于其左子树中每一个节点的键值，并小于其右子树中每一个节点的键值；
*		特点:		从根节点一直往左走，直至无左路可走就是最小元素
*						从根节点一直往右走，直至无右路可走就是最大元素
*						元素也是不能重复
*		缺点:		元素的插入和删除，可能会出现单个节点深度（根节点到该节点的路径）过大
*      操作:		插入新元素的时候先从根节点开始；
*						删除分两种情况
*
*	balanced binary search tree(平衡二叉搜索树)
*		来由:		从名字上定义就可以知道，是为了避免二叉搜索树出现不平衡（节点深度过大）的情况，导致搜索效率下降
*      优点:        可以保持一直平衡，搜索元素时间比较少
*      缺点:		插入节点和删除节点的平均时间比较长
*
*	AVL tree
*     定义：				一种特殊的 平衡二叉搜索树
*	   平衡条件：		确保整棵树的深度为O（logN）
*      操作:				单旋转，双旋转
*/
/************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;


/************************************************************************/
/* 定义                                                                     */
/************************************************************************/
// define data type for the key
typedef int keyType;

// define binary search tree data structure
struct BinaryTreeNode {
	keyType key;
	BinaryTreeNode* left; // left child
	BinaryTreeNode* right; // right child
};

// alias for the tree
typedef BinaryTreeNode bstree;

// alias for the tree node
typedef BinaryTreeNode bstnode;


/************************************************************************/
/* 查找，与顺序表的二分法思想一致                                                                     */
/************************************************************************/
// search BST by the given key
bstnode* search_by_key(bstree* tree, keyType key) {
	bstnode* node = tree;
	int found = 0;
	while (NULL != node) {
		if (key == node->key) {
			found = 1;
			break;
		}
		node = (key > node->key) ? node->right : node->left;
	}
	return found ? node : NULL;
}


/************************************************************************/
/* 向BST中插入一个key，不能破坏BST的规则                                                     */
/************************************************************************/
int insert_key(bstree* &tree, keyType key) 
{
	// 空树，则插入节点为根节点
	if (NULL == tree) {
		tree = (bstnode*)malloc(sizeof(bstnode));
		memset(tree,0,sizeof(bstnode));								// 注意清空
		tree->key = key;
		return 1;
	}

	// 查看节点是否已经存在,并且找到最小节点
	int found = 0;
	bstnode* curr = tree;
	bstnode* prev = NULL;
	while (NULL != curr) {
		// if already exists
		if (key == curr->key) {
			found = 1;
			break;
		}
		prev = curr;
		curr = (key > curr->key) ? curr->right : curr->left;
	}

	// 在最后节点插入key值
	if (!found && NULL == curr) {
		curr = (bstnode*)malloc(sizeof(bstnode));
		curr->key = key;
		((key > prev->key) ? prev->right : prev->left) = curr;
		return 1;
	}
	return 0;
}


/************************************************************************/
/* 删除key ：在BST中删除一个key，不能破坏BST的规则
*/
/************************************************************************/
int delete_key(bstree* &tree, keyType key) {
	// 设定临时树根，其key假设为无穷大，防止出现删除root的情况
	// 主要作用是让原来树的root可以像其他节点一样参与运算
	bstnode* head = (bstnode*)malloc(sizeof(bstnode));
	head->left = tree;									// 因为无穷大，所以原来的根节点肯定是在现在的 左子树


	// 查看是否存在BST 里面
	bstnode *curr = tree, *prev = head;
	bstnode *t1 = NULL, *t2 = NULL;
	int found = 0;
	while (NULL != curr) {
		if (key == curr->key) {
			found = 1;
			break;
		}
		prev = curr;
		curr = ((key > curr->key) ? curr->right : curr->left);
	}

	//分三种情况， 有左子树、有右子树、左右子树都有
	if (found) 
	{
		if (NULL == curr->left) 
		{
			// when the left child of the node is NULL
			((curr == prev->left) ? prev->left : prev->right) = curr->right;      // 直接把右子树的放到原来 curr的位置
			free(curr);
		}
		else if (NULL == curr->right) 
		{ 
			// when the right child of the node is NULL
			((curr == prev->left) ? prev->left : prev->right) = curr->left;
			free(curr);
		}
		else 
		{ 
		   //when the node has two children
		  // 按照二叉查找树的特性，保持中序遍历顺序即可， 这个可以通过画图，然后仔细看一下就能够知道，
		  // 即用该节点的中序前序后者后继 替代该节点即可  
			t1 = curr->left;
			while (NULL != t1->right) {
				t2 = t1;
				t1 = t1->right;
			}													// 一直寻找根的左子树的最大元素（从根节点一直往右走，直至无右路可走就是最大元素）
			curr->key = t1->key;
			((NULL == t2) ? curr->left : t2->right) = t1->left;
			free(t1);
		}
	}

	// 还原树根
	tree = head->left;
	free(head);

	return found;
}



/************************************************************************/
/*  递归算法，遍历                                                                     */
/************************************************************************/
//输出
void Visit(bstree* T)
{
	if (T->key != '#') {
		printf("%d ", T->key);
	}
}

//先序遍历， 根左右
void PreOrder(bstree* T)
{
	if (T != NULL) {
		//访问根节点
		Visit(T);
		//访问左子结点
		PreOrder(T->left);
		//访问右子结点
		PreOrder(T->right);
	}
}

//中序遍历  左根右
void InOrder(bstree* T)
{
	if (T != NULL) {
		//访问左子结点
		InOrder(T->left);
		//访问根节点
		Visit(T);
		//访问右子结点
		InOrder(T->right);
	}
}

//后序遍历  左右根
void PostOrder(bstree* T) {
	if (T != NULL) {
		//访问左子结点
		PostOrder(T->left);
		//访问右子结点
		PostOrder(T->right);
		//访问根节点
		Visit(T);
	}
}


int main() {

	cout << "插入构建二叉查找树：4 2 5 3 1 6" << endl;
	bstree * bst = NULL;
	insert_key(bst, 4);
	insert_key(bst, 2);
	insert_key(bst, 5);
	insert_key(bst, 3);
	insert_key(bst, 1);
	insert_key(bst, 6);
	cout << endl;
	cout << "遍历二叉查找树 ：" << endl << "前序\t : ";
	PreOrder(bst);
	cout << endl << "中序\t : ";
	InOrder(bst);
	cout << endl;
	cout << endl;

	cout << "删除元素 4 ： ";
	int r = delete_key(bst, 4);
	if (r) {
		cout << "Success";
	}
	else {
		cout << "Failed";
	}
	cout << endl;
	cout << "删除元素 32 ： ";
	r = delete_key(bst, 32);
	if (r) {
		cout << "Success";
	}
	else {
		cout << "Failed";
	}
	cout << endl;

	cout << endl;
	cout << "刪除之后 ：" << endl << "前序\t : ";
	PreOrder(bst);
	cout << endl << "中序\t : ";
	InOrder(bst);
	cout << endl;

	//
//	destory_btree(bst);

	getchar();

	return 0;
}


