/************************************************************************/
/* 
*  基本概念：			关联型容器，多键集合（multiset）
*	特点：					可以包含重复的元素，元素同样的经过排列的，与set的区别就是是否可以包含相同的元素
*  实现原理：		实现 红黑树（二叉树的一种）,使用的是链式存储；
*  使用场景：		
/
/************************************************************************/

#include <set>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
	multiset<int> c1;
	int ai[] = {0,  0, 1 , 12, 34, 6,8,9};
	multiset<int> c2(ai,ai+8);
	multiset<int> c3(c2);				// 采用拷贝构造函数
	multiset<int>::iterator Iter;
	multiset<int>::reverse_iterator RevIter;
	return 0;
}
