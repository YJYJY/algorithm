/************************************************************************/
/* STL  set的基本概念、特点、实现原理、使用方式，使用场景

基本概念：		一种关联容器，翻译过来就是 一种集合，一个有序的排列
特点：				自动将元素排序,（从小到大）不会包含重复的元素(唯一性)；
						不需要进行内存拷贝
限制：				不能直接改变元素的值；要改变必须先删除旧元素，然后插入新元素
使用场景：		查找一个元素是否在某集合内存中
实现原理：		实现 红黑树（二叉树的一种）,使用的是链式存储；
http://www.cnblogs.com/BeyondAnyTime/archive/2012/08/13/2636375.html
http://blog.csdn.net/acmmmm/article/details/21741541
*/
/************************************************************************/

#include <iostream>
#include <set>
#include <string>

using namespace std;

void SetChar()
{
	set<char>  ctr2;
	ctr2.insert('a');
	ctr2.insert('b');

}


void SetInt()
{
	set<int> s;
	s.insert(1);
	s.insert(2);
	s.insert(3);
	s.insert(1);
	cout << "set 的 size 值为 ：" << s.size() << endl;                 // 插入重复的值无效
	cout << "set 的 maxsize的值为 ：" << s.max_size() << endl;
	cout << "set 中的第一个元素是 ：" << *s.begin() << endl;
	cout << "set 中的最后一个元素是:" << *s.end() << endl;

	cout << "set 中 1 出现的次数是 ：" << s.count(1) << endl;       //某个某个键值出现的次数,不能重复，所以此函数无意义
	s.clear();
	if (s.empty())
	{
		cout << "set 为空 ！！！" << endl;
	}
	cout << "set 的 size 值为 ：" << s.size() << endl;
	cout << "set 的 maxsize的值为 ：" << s.max_size() << endl;
	getchar();
}

/************************************************************************/
/*equal_range 
分别表示第一个大于或等于给定关键值的元素和 第一个大于给定关键值的元素 */
/************************************************************************/
void Set_equal_range()
{
	set<int> s;
	set<int>::iterator iter;
	for (int i = 1; i <= 5; ++i)
	{
		s.insert(i);
	}
	for (iter = s.begin(); iter != s.end(); ++iter)
	{
		cout << *iter << " ";
	}
	cout << endl;
	pair<set<int>::const_iterator, set<int>::const_iterator> pr;
	pr = s.equal_range(3);
	cout << "第一个大于等于 3 的数是 ：" << *pr.first << endl;
	cout << "第一个大于 3的数是 ： " << *pr.second << endl;
	getchar();
}


void Set_erase()
{
	set<int> s;
	set<int>::const_iterator iter;
	set<int>::iterator first;
	set<int>::iterator second;
	for (int i = 1; i <= 10; ++i)
	{
		s.insert(i);
	}
	//第一种删除
	s.erase(s.begin());
	//第二种删除
	first = s.begin();
	second = s.begin();
	second++;
	second++;
	s.erase(first, second);
	//第三种删除,直接删除key_value
	s.erase(8);
	cout << "删除后 set 中元素是 ：";
	for (iter = s.begin(); iter != s.end(); ++iter)
	{
		cout << *iter << " ";
	}
	cout << endl;

	getchar();
}


void Set_find()
{
	int a[] = { 1,2,3 };
	set<int> s(a, a + 3);
	set<int>::iterator iter;
	if ((iter = s.find(2)) != s.end())
	{
		cout << *iter << endl;
	}
	getchar();
}


void Set_lower_upper_bound()
{
	set<int> s;
	set<int>::iterator iter;
	s.insert(1);
	s.insert(3);
	s.insert(4);
	s.insert(40);
	s.insert(489);
	s.insert(98009);

	cout << "set 的 size 值为 ：" << s.size() << endl;                 // 插入重复的值无效

	for (iter = s.begin(); iter != s.end(); ++iter)
	{
		cout << *iter << " ";
	}
	cout << endl;


	cout << *s.lower_bound(2) << endl;		// 返回第一个大于等于key_value的定位器
	cout << *s.lower_bound(10) << endl;
	cout << *s.upper_bound(10) << endl;   // 返回第一个大于key的迭代器指针
	getchar();

}


/************************************************************************/
/*      使用自定义的 数据类型作为Set 的参数       
http://www.cnblogs.com/hicjiajia/archive/2010/12/18/1909998.html*/
/************************************************************************/
void Set_Advance_Custom_types_one()
{
	struct Student {     // 存储学生的姓名及四级分数
		string name;
		int total;
	};

	class PS {     // 比较对象
	public:
		bool operator()(const Student& st1, const Student& st2)   // 写operator()函数
		{
			return st1.name < st2.name;
		}
	};


	Student student[3] = {
		{ "hicjiajia",425 },
		{ "jintiankaosijile",425 },
		{ "buzhidaonengbunengguo",425 }
	};
	set<Student, PS> st;    // 用自定义的比较对象（PS）声明一个set对象
	st.insert(student[0]);
	st.insert(student[1]);
	st.insert(student[2]);

	set<Student>::iterator it = st.begin();
	for (; it != st.end(); it++)
	{
		cout << (*it).name << " " << (*it).total << endl;
	}

	system("pause");


}

void Set_Advance_Custom_types_two()
{
	struct Student {
		string name;
		int total;
		bool operator<(const Student& st)const   //给出operator<()的定义，以便在插入容器时排序 
		{
			return this->name < st.name;
		}
	};

	Student student[3] = {
		{ "hicjiajia",425 },
		{ "jintiankaosijile",425 },
		{ "buzhidaonengbunengguo",425 }
	};
	set<Student> st;      //注意这里，与第一段代码的写法区别 
	st.insert(student[0]);
	st.insert(student[1]);
	st.insert(student[2]);

	set<Student>::iterator it = st.begin();
	for (; it != st.end(); it++)
	{
		cout << (*it).name << " " << (*it).total << endl;
	}

	system("pause");
}





int main()
{
//	SetInt();
//	Set_equal_range();
//	Set_erase();
	//Set_find();
//	Set_lower_upper_bound();
	Set_Advance_Custom_types_one();
	Set_Advance_Custom_types_two();
	return 0;
}