/************************************************************************/
/* 对于一些有疑问的 window api 进行测试                                                             */
/************************************************************************/


#include <iostream>
#include <sstream>
#include <windows.h>
#include <time.h>
#include <io.h>
#include <assert.h>


using namespace std;

void Test_systime()
{
	/*https://msdn.microsoft.com/en-us/library/windows/desktop/ms724936(v=vs.85).aspx*/
	SYSTEMTIME begin;
	FILETIME fbegin;
	GetLocalTime(&begin);

	SystemTimeToFileTime(&begin, &fbegin);
	while (1)
	{
		// wait max 10 sec
		SYSTEMTIME now;
		FILETIME fnow;
		GetLocalTime(&now);

		//		now.wDay -= 2;
		//		SetLocalTime(&now);               // 设置系统时间，正常看到的时间
		//		SetSystemTime(&now);			// 设置 utc 时间
		SystemTimeToFileTime(&now, &fnow);

		//		LPFILETIME fcompare = fnow - fbegin;

		//		fnow->
		Sleep(2000);
	}
}


void Test_CTime()
{
	//http://www.cnblogs.com/hushaojun/p/5639780.html
	time_t start, end;
	double cost;

	time(&start);
	Sleep(2000);
	time(&end);
	cost = difftime(end, start);

	if (cost >= 2.0)
	{
		printf("%f\n", cost);

	}
	printf("%f\n", cost);
}

void Test_GetTickCount()
{
	double start = GetTickCount();
	Sleep(1000);
	double end = GetTickCount();
	cout << "GetTickCount:" << end - start << endl;
}

void Test_nanosleep()
{

	struct timespec req, rem;
	req.tv_sec = (time_t)1;
	req.tv_nsec = ((long)1000);
	//	nanosleep(&req, &rem);  



}

void Test_access()
{
	int aabb = 0;
	cout << "test access start " << endl;
	// 如果 路径不存在，那么创建他
	if (-1 == _access("../qwe", 0))
	{
		CreateDirectoryA("../qwe", NULL);
	}
}

void Test_time()
{
	char szTime[64] = { 0 };
	time_t timeData = -1;
	//	timeData = time(NULL);
	struct tm *p = localtime(&timeData);
	p->tm_year += 1900;
	p->tm_mon += 1;
	snprintf(szTime, 64, "%04d-%02d-%02dT%02d:%02d:%02d",
		p->tm_year, p->tm_mon, p->tm_mday, p->tm_hour, p->tm_min, p->tm_sec);
	cout << szTime << endl;
	cout << "dsadsad" << endl;
}



void MySetLocalTime(IN const char* const &pDate)
{//May be here get root-auth
	char *pBeginPos = (char *)pDate;
	char *pPos = strstr(pBeginPos, "-");
	struct tm date;
	struct timeval now;

	if (NULL == pPos) return ;

	memset(&now, 0, sizeof(now));
	memset(&date, 0, sizeof(date));
	date.tm_year = atoi(pBeginPos) - 1900;
	date.tm_mon = atoi(pPos + 1) - 1;
	pPos = strstr(pPos + 1, "-");
	if (NULL == pPos) return ;

	date.tm_mday = atoi(pPos + 1);
	pPos = strstr(pPos + 1, "T");
	if (NULL != pPos)
	{
		date.tm_hour = atoi(pPos + 1);
		pPos = strstr(pPos + 1, ":");
		if (NULL != pPos)
		{
			date.tm_min = atoi(pPos + 1);
			pPos = strstr(pPos + 1, ":");
			if (NULL != pPos)
			{
				date.tm_sec = atoi(pPos + 1);
				pPos = strstr(pPos + 1, ".");
				if (NULL != pPos)
				{
					now.tv_usec = 1000 * (atoi(pPos + 1));
				}
			}
		}
	}

#ifndef WIN32
	now.tv_sec = mktime(&date);
	settimeofday(&now, NULL);
#else
	SYSTEMTIME setLocal;


	setLocal.wYear = date.tm_year + 1970;
	setLocal.wMonth = date.tm_mon + 1;
	setLocal.wDay = date.tm_mday;
	setLocal.wHour = date.tm_hour;
	setLocal.wMinute = date.tm_min;
	setLocal.wSecond = date.tm_sec;
	::SetLocalTime(&setLocal);
#endif 

	return ;
}

FILE* test = fopen("time.txt", "wb");
FILE* test1 = fopen("time1.txt", "wb");
DWORD WINAPI Test_GetLocalTime(void *p)  //演示函数，将会在新创建的线程中运行的代码
{
	while (1)
	{
		// wait max 10 sec
		SYSTEMTIME stLocal;
		GetLocalTime(&stLocal);

		char date[128] = { 0 };
		memset(date, 0, sizeof(date));
		snprintf(date, sizeof(date), "%04d-%02d-%02dT%02d:%02d:%02d.%03d \r\n",
			stLocal.wYear, stLocal.wMonth, stLocal.wDay, stLocal.wHour, stLocal.wMinute, stLocal.wSecond, stLocal.wMilliseconds);

		fwrite(date, 1, strlen(date), test);

		MySetLocalTime(date);
		Sleep(1000);
	}
	return 0;
}

void Test_char(char* pTmp)
{
	char szTmp[64] = { 0 };
	stringstream ss;
	int j = 0;
	int k = 0;
	while ('v' != pTmp[j++])
	{
	}

	j++;
	k = 0;
	memset(szTmp, 0, 64);
	while ('/' != pTmp[j])
		szTmp[k++] = pTmp[j++];

	cout << atoi(szTmp) << endl;

	j++;
	k = 0;
	memset(szTmp, 0, 64);
	while ('/' != pTmp[j])
		szTmp[k++] = pTmp[j++];
	cout << atoi(szTmp) << endl;

	j++;
	k = 0;
	memset(szTmp, 0, 64);
	while ('/' != pTmp[j])
		szTmp[k++] = pTmp[j++];
	cout << atoi(szTmp) << endl;

	j++;
	k = 0;
	memset(szTmp, 0, 64);
	while ('/' != pTmp[j])
		szTmp[k++] = pTmp[j++];
	cout << atoi(szTmp) << endl;

	j++;
	k = 0;
	memset(szTmp, 0, 64);
	while ('/' != pTmp[j])
		szTmp[k++] = pTmp[j++];
	cout <<"dsadsada:---" << atoi(szTmp) << endl;

	
	j++;
	k = 0;
	memset(szTmp, 0, 64);
	while (('/' != pTmp[j]) && ('\0' != pTmp[j]))
		szTmp[k++] = pTmp[j++];
	cout << atoi(szTmp) << endl;

	j++;
	k = 0;
	memset(szTmp, 0, 64);
	while (('/' != pTmp[j]) && ('\0' != pTmp[j]))
		szTmp[k++] = pTmp[j++];
	cout << atoi(szTmp) << endl;

	j++;
	k = 0;
	memset(szTmp, 0, 64);
	while ('\0' != pTmp[j])
		szTmp[k++] = pTmp[j++];
	cout << atoi(szTmp) << endl;
}


void Test_basic()
{

}

bool happened(double probability)//probability 0~1
{
	const double MinProb = 1.0 / ((long)1 + RAND_MAX);

	if (probability <= 0)
		return false;
	if (probability < MinProb)
		return ((rand() == 0) && (happened(probability * ((long)1 + RAND_MAX))));
	if (rand() <= (probability*((long)1 + RAND_MAX)))
		return true;
	return false;
}

long GetRandomValue(const long &n)//产生0~n-1之间的等概率随机数
{
	long t = 0;
	if (RAND_MAX >= n)
	{
		long r = RAND_MAX - ((long)1 + RAND_MAX) % n;//尾数
		t = rand();
		while (t > r)
			t = rand();

		return t % n;
	}
	else
	{
		long r = n % ((long)1 + RAND_MAX);//余数
		if (happened((double)r / n))//取到余数的概率
			return (n - r + GetRandomValue(r));
		else
			return (rand() + GetRandomValue(n / ((long)1 + RAND_MAX))*((long)1 + RAND_MAX));
	}
}


DWORD GetDeviceType(string strDID)
{
	string strtype = strDID.substr(10, 3);
	DWORD ntype = atoi(strtype.c_str());
	switch (ntype)
	{

	default:
		break;
	}

	return 0;
}

int main()
{

	//	Test_CTime();
	//Test_GetTickCount();
	//Test_systime();
	//Test_char("v/134/23/367/434/532a/643/724/348");
#if 0

	string strStartTime = "2015-07-25T21:10:39";
	size_t pos = strStartTime.find("T");
	string strDate = strStartTime.substr(0, strStartTime.find("T"));

	int a, b, c, d, e, f, g;
	sscanf(strStartTime.c_str(), "%04d-%02d-%02dT%02d:%02d:%02d", &a, &b, &c, &e, &f, &g);

	DWORD year = 0;
	DWORD mon = 0;
	DWORD day = 0;
	sscanf(strStartTime.c_str(), "%04d-%02d-%02d", &year, &mon, &day);
//	DWORD DWDate = year << 16 + mon << 8 + day;
	DWORD DWDate = (year << 16)|(mon<<8)|(day);

	cout << DWDate << endl;
	WORD nyear = DWDate >> 16;
	BYTE month = BYTE(DWDate >> 8);
	BYTE bday = BYTE(DWDate);
#endif


	/*HANDLE threadhandle = CreateThread(NULL, 0, Test_GetLocalTime, NULL, 0, NULL);
	while (1)
	{
		// wait max 10 sec
		SYSTEMTIME stLocal;
		GetLocalTime(&stLocal);

		char date[128] = { 0 };
		memset(date, 0, sizeof(date));
		snprintf(date, sizeof(date), "%04d-%02d-%02dT%02d:%02d:%02d.%03d \r\n",
			stLocal.wYear, stLocal.wMonth, stLocal.wDay, stLocal.wHour, stLocal.wMinute, stLocal.wSecond, stLocal.wMilliseconds);

		fwrite(date, 1, strlen(date), test1);

		MySetLocalTime(date);

		Sleep(1000);
	}
*/

	string strDID = "34020000002000000001";
	GetDeviceType(strDID);

	getchar();


	return 0;
}