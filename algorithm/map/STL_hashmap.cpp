/************************************************************************/
/* 简单使用 STL 库 的 hash_map
*  hashtable.cpp 是 hash_map 的 具体实现方式

* 特点：				哈希表最大的优点，就是把数据的存储和查找消耗的时间大大降低，几乎可以看成是常数时间；
							而代价仅仅是消耗比较多的内存，比 map 快出很多； 
* 存储结构：				hash表存储
* 使用场景:			①考虑效率，特别是在元素达到一定数量级时，考虑考虑hash_map
							②内存使用情况

http://blog.csdn.net/yusiguyuan/article/details/12883727
*/
/************************************************************************/

#if __GNUC__
#include <ext/hash_map>
using namespace __gnu_cxx;
#else
#include <hash_map>
using namespace stdext;
#endif
#include <string>
#include <utility>
#include <iostream>
using namespace std;

void testHaspMap()
{

	hash_map< const char*, const char*> test;

	test.insert(make_pair("5", "5000"));
	test.insert(make_pair("2", "2000"));
	test.insert(make_pair("ccc", "ccc"));
	test.insert(make_pair("7", "7000"));
	test.insert(make_pair("1", "1000"));
	test.insert(make_pair("4", "4000"));
	test.insert(make_pair("3", "3000"));
	test.insert(make_pair("6", "6000"));
	test.insert(make_pair("abc", "abc"));
	test.insert(make_pair("8", "8000"));
	test.insert(make_pair("9", "9000"));

	cout << test.size() << endl;
	hash_map< const char*, const char*>::iterator it = test.begin();
	for (; it != test.end(); it++)
	{
		cout << it->first << "--" << it->second << endl;
	}

	if (test.find("ccc") != test.end())
	{
		hash_map< const char*, const char*>::iterator it = test.find("ccc");
		cout << it->second << endl;
		test.erase(it);      // 擦除元素
	}

}

int main()
{
	testHaspMap();
	getchar();
	return 0;
}

