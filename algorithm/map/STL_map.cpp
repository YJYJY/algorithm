/************************************************************************/
/*  测试 stl map 的 使用                                                                     
*   关联型容器：  在插入时候就会进行排序，所以要求比较大小
*
*/


/************************************************************************/

#include<map>
#include <string>
#include <iostream>

using namespace std;

/************************************************************************/
/*     map 的 key 值存放自定的数据类型的时候， 需要重载  <  , 否则会提示错误
*	  因为 map 插入 或者删除数据的时候都需要重新进行排序， 排序就要比较大小， 因此 要重载 <
*/
/************************************************************************/ 
#if 0
void Test_map_custom()
{
	typedef struct MyStruct
	{
		int nOne;
		char strName[32];
	}MAP_key_custom;

	typedef map<MAP_key_custom, int>  MYMAP;
	typedef MYMAP::iterator  MYMAP_IT;
	typedef MYMAP::const_iterator MYMAP_CIT;

	MYMAP InstantMap;

	MAP_key_custom KeyInt;
	memset(&KeyInt, 0, sizeof(MAP_key_custom));
	InstantMap.insert(make_pair(KeyInt,10));               //  前面都提示正常，但是执行这个 插入操作的时候， 链接的时候就会提示没有重载

}
#endif




void Test_map_operator()
{
	typedef map<int, int>  MYMAP;
	typedef MYMAP::iterator  MYMAP_IT;
	typedef MYMAP::const_iterator MYMAP_CIT;

	MYMAP testmap;
	testmap.insert(make_pair(1, 1));
	testmap.insert(make_pair(2, 2));
	testmap.insert(make_pair(3, 3));
	testmap.insert(make_pair(4, 4));


	MYMAP mapCopy;
	mapCopy = testmap;
	cout << mapCopy.size() << endl;


	MYMAP mapCopyConstruct(testmap);
	cout << mapCopyConstruct.size() << endl;





}


int main()
{
//	Test_map_custom();
	Test_map_operator();
	return 0;
}