/************************************************************************/
/* queue （队列是一个类型的，先进先出）
*配接器 		
*特点：				先进先出
*实现：				通过序列容器deque或者list对象来实现,不能通过 vector 实现
* 注意：				与stack 是一个类型，（操作差不多）， 但是与 deque 不是一个类型
*/

/*priority_queue (优先队列、堆)
*一种按值排序的队列容器，按照优先级次序从队列中弹出元素
*特点： 已经排序好
*/

/************************************************************************/
#include <queue>
#include <iostream>

using namespace std;

void Test_queue()
{
	queue<int>	qInt;

	qInt.push(10);			 // 从头添加元素
	qInt.pop();				 // 从尾 删除元素

	int queFront = qInt.front();		// 获取当前头元素

	getchar();
}

void Test_ProrityQueue()
{
	priority_queue<int> PQInt;

	PQInt.push(1);
	PQInt.push(19);
	PQInt.push(129);
	PQInt.push(139);
	PQInt.push(119);
	PQInt.push(49);
	PQInt.push(152);
	PQInt.push(21332);                     // 插入的时候已经 排序好

	cout << PQInt.size() << endl;

	int PQSize = PQInt.size();

	for (int i=0; i<PQSize; i++)
	{
		cout << PQInt.top() << endl;
		PQInt.pop();
	}

	getchar();
}

int main()
{
//	Test_queue();
	Test_ProrityQueue();
	return 0;
}