/************************************************************************/
/* 单向链表的一些基本操作                                                                     */
/************************************************************************/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
	struct Node* next;
	int value;
}Node,*List;

void print(List a)
{
	if (!a) return;
	Node *p = a->next;
	while (p)
	{
		printf("%d ", p->value);
		p = p->next;
	}
	putchar('\n');
}

// 创建单向链表
List CreateList(int a[], int len)
{
	int i;
	Node *head = (Node*)malloc(sizeof(Node));
	Node *p = head;
	head->next = NULL;
	for (i = 0; i < len; i++)
	{
		Node *n = (Node*)malloc(sizeof(Node));
		n->value = a[i];
		p->next = n;
		p = n;
	}
	p->next = NULL;
	return head;
}

// 销毁单向链表
void DestoryList(List a)
{

}

// 合并两个单向链表,并且单调递增
List MergeList(List a, List b)
{
	Node *p, *pa, *pb, *prea;
	if (a == NULL || b == NULL) return NULL;

	p = a;		//用于返回链表头
	prea = a;
	pa = a->next;
	pb = b->next;
	while (pa && pb)
	{
		if (pa == pb)
		{
			pb = NULL;
			break;
		}
		else if (pa->value > pb->value)
		{
			//注意两个节点的值相等的时候还不能插进来，防止a节点后的地3个节点与b节点后的第一个节点是同一个节点，
			//这时如果a节点后的第1，2个节点与第3个节点（b后的第1个节点）的值相等的话，那么会把b后第1个节点插进来，
			//这样就不能等到a的第3个节点与b的第1个节点比较地址了。
			prea->next = pb;
			pb = pb->next;
			prea = prea->next;
			prea->next = pa;
		}
		else
		{
			if (pa->next == pb->next)
			{
				//要判断一下下一个节点是否相等，如果a链表所有节点是0，b链表的第一个节点是0，
				//并且与a链表的节点有交集，如果不判断，这里的最后结果是把b放在a后面，
				//这样b的最后有指向a前面那个交集的节点，就会形成一个环了。
				pb->next = NULL;
			}
			prea = pa;
			pa = pa->next;
		}
	}

	prea->next = pa ? pa : pb;
	return p;
}

// 合并两个单向链表,并且单调递增
Node * MergeList_two(Node *head1, Node *head2)
{
	if (head1 == NULL)
		return head2;
	if (head2 == NULL)
		return head1;
	Node *head = NULL;
	Node *p1 = NULL;
	Node *p2 = NULL;

	if (head1->value < head2->value)          //找出链表头
	{
		head = head1;
		p1 = head1->next;
		p2 = head2;
	}
	else
	{
		head = head2;
		p2 = head2->next;
		p1 = head1;
	}
	Node *pcurrent = head;

	while (p1 != NULL && p2 != NULL)     // 循环比较大小
	{
		if (p1->value <= p2->value)
		{
			pcurrent->next = p1;
			pcurrent = p1;
			p1 = p1->next;
		}
		else
		{
			pcurrent->next = p2;
			pcurrent = p2;
			p2 = p2->next;
		}
	}

	if (p1 != NULL)
		pcurrent->next = p1;
	if (p2 != NULL)
		pcurrent->next = p2;
	return head;
}

// 单向链表的逆序,循环法
Node *ReverseList(Node *stu)
{
	if (stu == NULL || stu->next == NULL)    //链表为空或者只有一个元素不需要进行逆序
		return stu;

	Node *p1, *p2, *p3;

	p1 = stu->next;                           //p1指向链表头节点的下一个节点
	p2 = p1->next;                           //p2 保存链表的第二个节点， 下面的操作始终是通过操作 相邻的这两个节点，来 逆序
	p1->next = NULL;                       // 逆序后，原来的第一个节点的 next（下一个节点）一定是 空

										       /*基本思想就是，通过相邻两个节点来做操*/
	while (p2)                            // 下一个节点不为空，则表明后面还有元素，继续逆序
	{
		p3 = p2->next;               // 临时保存，第三个节点，
		p2->next = p1;              //把第二个节点的 next 指向回 第一个节点， 这样就是 逆序了  
		p1 = p2;                         // 后面这两步操作就是相当于为 两个节点往后 移动了一个位置，为下一次循环做准备
		p2 = p3;
	}

	stu->next = p1;                           //将链表头节点指向p1
	return stu;
}

//检测单向链表是否存在环
/************************************************************************/
/* 设置两个指针pSlow，pFast，慢的一次跳一步，快的一次跳两步，往链表末端移动，若慢指针追上了快指针
，则说明链表存在环。此方法速度很快且不需要额外的存储空间。
①求环长，固定一个指针，另外一个指针继续移动，再次相等则是环长；
②求环前面的长度，快慢指针都回到原点，然后先让一个指针走环的长度，然后再同时走，相遇时就可以得到之前的长度了；
   */
/************************************************************************/
bool IsLoop(Node* head)
{
	Node *pSlow = head;
	Node *pFast = head;
	while (pSlow != NULL && pFast != NULL)
	{
		pSlow = pSlow->next;
		pFast = pFast->next->next;
		if (pSlow == pFast)
		{
			//相等后求环长
			pSlow = pSlow->next;
			int i = 1;
			while(pSlow != pFast)
			{
				pSlow = pFast->next;
				i++;
			}
			return true;
		}
	}
	return false;
}


int main()
{

	int a[] = { 1, 2, 3, 4 };
	int b[] = { 0, 1, 6 };
	List la = CreateList(a, 4);
	List lb = CreateList(b, 3);
	print(la);
	print(lb);
//	print(MergeList_two(la, lb));
//	print(MergeList(la, lb));     // 一次只能测试一个
	print(ReverseList(la));

	getchar();

	return 0;
}