/************************************************************************/
/* list 
*	序列容器
* 优势：				在任何 位置执行插入和删除动作都非常迅速，比vector和 deque
* 实现原理：		实现 双向链表
* 缺点：				不能支持随机访问 数据元素；
*/
/************************************************************************/

#include <list>
#include <iostream>

using namespace std;

void test_list_itorer()
{
	typedef char CHAR;
	typedef struct
	{
		CHAR RecordName[64];                                // 录像名称
		CHAR StartTime[64];                                 // 录像开始时间 2014-06-21T18:12:06
		CHAR EndTime[64];                                   // 录像结束时间
		CHAR Addr[128];                                     // 录像地址
		CHAR SourceId[128];                                 // 资源ID
		CHAR Description[128];                              //资源描述
	}RECORDIND_INFORMATION_RESPONSE_ITEM;

	typedef list<RECORDIND_INFORMATION_RESPONSE_ITEM*> LIST_RECORDINFO;				// 存放一天中的多个录像片段信息
	typedef LIST_RECORDINFO::iterator	LIST_RECORDINFO_IT;
	typedef LIST_RECORDINFO::const_iterator LIST_RECORDINFO_CIT;

	LIST_RECORDINFO*  RecInfo = new LIST_RECORDINFO;
	for (int i=0;i<3;i++)
	{
		RECORDIND_INFORMATION_RESPONSE_ITEM* item = new RECORDIND_INFORMATION_RESPONSE_ITEM;
		memset(item, 0, sizeof(RECORDIND_INFORMATION_RESPONSE_ITEM));
		sprintf(item->StartTime, "%04d-%02d-%02dT%02d:%02d:%02d", (i + 2 * i), (i + 3 * i), (i + 4 * i), (i + 5 * i), (i + 6 * i), (i + 7 * i));
		RecInfo->push_back(item);
	}
	
	LIST_RECORDINFO_IT itor = RecInfo->begin();
	for (; itor != RecInfo->end(); itor++)
	{
		RECORDIND_INFORMATION_RESPONSE_ITEM* item = *itor;
		int year, month, day, hour, min, second;
		cout << item->StartTime << endl;
		sscanf(item->StartTime, "%04d-%02d-%02dT%02d:%02d:%02d", &year, &month, &day, &hour, &min, &second);
		cout << year << "-" << month << "-" << day << "T" << endl;
	}

}

void test_basic()
{
	list<int> lInt;
	lInt.push_back(1);
	lInt.pop_back();
	lInt.push_front(2);
	lInt.pop_front();


	list<int>::iterator lIter = lInt.begin();
	lInt.insert(lIter, 2);

	lInt.erase(lIter);						// 删除特定元素
	lInt.clear();						    // 删除所有元素
}


int main()
{
	//test_basic();
	test_list_itorer();
	return 0;
}